HELM
argocd repo add https://bitbucket.org/tekkstone/kustomize.git
argocd app create ks-guestbook-demo --repo https://bitbucket.org/tekkstone/kustomize --path ./ks-guestbook/base --dest-server $MINIKUBE_IP --dest-namespace sample
