package de.bund.bamf.bp003.externalservice.service;

public class CorruptServiceImpl implements ICorruptService {

    private boolean isCorrupt = false;

    @Override
    public boolean isCorrupt() {
        return this.isCorrupt;
    }

    @Override
    public void setCorrupt(boolean isCorrupt) {
        this.isCorrupt = isCorrupt;
    }
}
