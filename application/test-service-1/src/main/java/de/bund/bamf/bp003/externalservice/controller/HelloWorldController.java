package de.bund.bamf.bp003.externalservice.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import de.bund.bamf.bp003.externalservice.HttpResponse;
import de.bund.bamf.bp003.externalservice.service.ICorruptService;

@RestController
@RequestMapping("/api")
public class HelloWorldController {

	@Value("${testdata.message}")
	String message;
	
    private final ICorruptService corruptService;

    public HelloWorldController(ICorruptService corruptService) {
        this.corruptService = corruptService;
    }

    @GetMapping(path = "/message")
    public String getMessage() {
    	return message;
    }
    
    @GetMapping(path = "/hello")
    @ResponseBody
    public ResponseEntity<HttpResponse> getHelloWorld() {
        if (corruptService.isCorrupt()) {
            return new ResponseEntity<>(new HttpResponse(HttpStatus.GATEWAY_TIMEOUT.value(), "gateway timeout"), HttpStatus.GATEWAY_TIMEOUT);
        } else {
            return new ResponseEntity<>(new HttpResponse(HttpStatus.OK.value(), "successful"), HttpStatus.OK);
        }
    }
}
