package de.bund.bamf.bp003.externalservice.config;

import de.bund.bamf.bp003.externalservice.service.CorruptServiceImpl;
import de.bund.bamf.bp003.externalservice.service.ICorruptService;
import org.springframework.context.annotation.Bean;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public ICorruptService corruptService() {
        return new CorruptServiceImpl();
    }
}
