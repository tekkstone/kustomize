package de.bund.bamf.bp003.externalservice.controller;

import de.bund.bamf.bp003.externalservice.HttpResponse;
import de.bund.bamf.bp003.externalservice.service.ICorruptService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class CorruptController {

    private final String secretValue = "1234";

    private final ICorruptService corruptService;

    public CorruptController(ICorruptService corruptService) {
        this.corruptService = corruptService;
    }

    @GetMapping(path = "/corrupt")
    @ResponseBody
    public ResponseEntity<HttpResponse> getCorrupt(@RequestParam String secret) {
        if (secret.equals(this.secretValue)) {
            this.corruptService.setCorrupt(true);
            return new ResponseEntity<>(new HttpResponse(HttpStatus.OK.value(), "service is now down."), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new HttpResponse(HttpStatus.FORBIDDEN.value(), "bad secret"), HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping(path = "/uncorrupt")
    @ResponseBody
    public ResponseEntity<HttpResponse> getUnCorrupt(@RequestParam String secret) {
        if (secret.equals(this.secretValue)) {
            this.corruptService.setCorrupt(false);
            return new ResponseEntity<>(new HttpResponse(HttpStatus.OK.value(), "service is now up again."), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(new HttpResponse(HttpStatus.FORBIDDEN.value(), "bad secret"), HttpStatus.FORBIDDEN);
        }
    }

}
