package de.bund.bamf.bp003.externalservice.service;

public interface ICorruptService {

    public boolean isCorrupt();
    public void setCorrupt(boolean isCorrupt);
}
