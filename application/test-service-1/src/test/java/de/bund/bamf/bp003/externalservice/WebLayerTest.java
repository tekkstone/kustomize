package de.bund.bamf.bp003.externalservice;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import de.bund.bamf.bp003.externalservice.controller.CorruptController;
import de.bund.bamf.bp003.externalservice.controller.HelloWorldController;
import de.bund.bamf.bp003.externalservice.service.CorruptServiceImpl;
import de.bund.bamf.bp003.externalservice.service.ICorruptService;


//@SpringBootTest(classes=App.class)
@ExtendWith(SpringExtension.class)
@WebMvcTest({HelloWorldController.class, CorruptController.class})
public class WebLayerTest {

    @Autowired
    private MockMvc mockMvc;

    @TestConfiguration
    static class EmployeeServiceImplTestContextConfiguration {

        @Bean
        public ICorruptService corruptService() {
            return new CorruptServiceImpl();
        }
    }

    @Test
    public void shouldReturnOkForHelloWorldService() throws Exception {
        this.mockMvc.perform(get("/api/uncorrupt?secret=1234"));
        this.mockMvc.perform(get("/api/hello")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void shouldReturnCorruption() throws Exception {
        this.mockMvc.perform(get("/api/uncorrupt?secret=1234"));
        this.mockMvc.perform(get("/api/hello")).andDo(print()).andExpect(status().isOk());
        this.mockMvc.perform(get("/api/corrupt?secret=1234")).andDo(print()).andExpect(status().isOk());
        this.mockMvc.perform(get("/api/hello")).andDo(print()).andExpect(status().isGatewayTimeout());
    }
}